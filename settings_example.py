#mEGAdata configuration
SECRET_KEY = 'top_secret_key'
APPLICATION_PORT = 5000
APPLICATION_HOST = "127.0.0.1"
APPLICATION_DEBUG_MODE = True

#Database configuration
DATABASE_NAME = 'db_name'
DATABASE_HOST = 'localhost'
DATABASE_USER = 'megadata_user'
DATABASE_PASSWORD = 'passwd'
